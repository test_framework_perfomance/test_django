from django.db import models
from django.db.models import JSONField

from apps.core.utility.serialisators import OrjsonDecoder, OrjsonEncoder


class TestData(models.Model):
    id = models.AutoField(primary_key=True)
    payload = JSONField(encoder=OrjsonEncoder, decoder=OrjsonDecoder)

    def __str__(self) -> str:
        return self.id
