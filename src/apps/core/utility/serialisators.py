import json
from typing import Any, Callable

import orjson
from django.core.serializers.json import DjangoJSONEncoder


class OrjsonEncoder(DjangoJSONEncoder):
    def default(self, obj) -> str:
        try:
            return orjson.dumps(obj).decode('utf-8')
        except orjson.JSONEncodeError as exc_info:
            raise TypeError() from exc_info


class OrjsonDecoder(json.JSONDecoder):
    def decode(self, s: str, _w: Callable | None = None) -> Any:
        try:
            result = orjson.loads(s.encode('utf-8'))
        except orjson.JSONDecodeError as exc_info:
            raise json.JSONDecodeError(
                msg='Ошибка при декодировании json', doc=s, pos=0
            ) from exc_info
        return result
