import logging
import random
import string
from abc import ABC, abstractmethod
from datetime import datetime as dt
from datetime import timedelta
from typing import Any

import pytz

from apps.core.models import TestData


class RepoBase(ABC):
    _temp_list_strings: list[str]
    _logger: logging.Logger

    def __init__(self):
        self._logger = logging.getLogger(__name__)

    async def generate_data(self, count_lines: int, count_columns: int = 100) -> None:
        batch = 1_000
        self._fill_temp_strings(batch)
        count_down = count_lines
        try:
            while count_down > 0:
                print(f'count_down = {count_down}')
                array_data = []
                count = min(count_down, batch)
                for _ in range(count):
                    array_data.append(
                        {'payload': self._get_new_json(count_columns=count_columns)}
                    )
                await self._add_data_to_db(data=array_data)
                count_down -= count
        except Exception as ex:
            self._logger.exception(ex)

        self._temp_list_strings = []

    @abstractmethod
    async def _add_data_to_db(self, data: list[dict[str, str]]) -> None:
        raise NotImplementedError('Not implemented method')

    @abstractmethod
    async def get_batch_data(
        self, from_index: int, batch_size: int = 10_000
    ) -> list[dict[str, Any]]:
        raise NotImplementedError('Not implemented method')

    @abstractmethod
    async def add_data(self, data: list[dict[str, str]]) -> bool:
        raise NotImplementedError('Not implemented method')

    @abstractmethod
    async def clear_table(self) -> None:
        raise NotImplementedError('Not implemented method')

    async def get_count_rows(self) -> int:
        return await TestData.objects.count()

    def _fill_temp_strings(self, batch: int) -> None:
        self._temp_list_strings = []
        chars = string.ascii_letters + string.digits
        for _ in range(batch * 2):
            string_length = random.randint(0, 1_000)
            self._temp_list_strings.append(
                ''.join(random.choice(chars) for _ in range(string_length))
            )

    def _get_new_json(self, count_columns: int = 100) -> dict[str, Any]:
        list_type_column = ['int', 'str', 'date']
        dict_json = {}
        date_start = dt.now(pytz.UTC)
        for i in range(count_columns):
            type_column = random.choice(list_type_column)
            column_name = f'column_{i}'
            if type_column == 'int':
                dict_json[column_name] = random.randint(0, 1_000_000_000)
            elif type_column == 'str':
                dict_json[column_name] = random.choice(self._temp_list_strings)
            elif type_column == 'date':
                dict_json[column_name] = date_start - timedelta(
                    seconds=random.randint(0, 1_000_000_000)
                )

        return dict_json


json_data_repo: RepoBase | None = None


def get_repo_json() -> RepoBase:
    global json_data_repo
    if json_data_repo is None:
        from apps.core.repos.repo_django_orm import JSONDataRepo
        from apps.core.repos.repos_asyncpg import RepoAsyncPG

        json_data_repo = RepoAsyncPG()
    return json_data_repo
