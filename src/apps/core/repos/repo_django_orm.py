import logging
import random
import string
from datetime import datetime as dt
from datetime import timedelta
from typing import Any

import pytz
from django.db.models.fields import TextField
from django.db.models.functions import Cast

from apps.core.models import TestData
from apps.core.repos.repo_base import RepoBase


class JSONDataRepo(RepoBase):
    async def _add_data_to_db(self, data: list[dict[str, str]]) -> None:
        await TestData.objects.abulk_create((TestData(**item) for item in data))

    async def get_batch_data(
        self, from_index: int, batch_size: int = 10_000
    ) -> list[dict[str, Any]]:
        try:
            query_set = TestData.objects.annotate(
                payload_str=Cast('payload', TextField())
            ).order_by('id')[from_index : from_index + batch_size]
            list_result = []
            async for item in query_set:
                list_result.append({'index': item.id, 'data': item.payload_str})

            return list_result
        except Exception as ex:
            self._logger.exception(ex)
            raise

    async def add_data(self, data: list[dict[str, str]]) -> bool:
        try:
            array_data = [TestData(payload=item) for item in data]
            await TestData.objects.abulk_create(array_data)
            return True
        except Exception as ex:
            self._logger.exception(ex)
            return False

    async def clear_table(self) -> None:
        await TestData.objects.adelete()
