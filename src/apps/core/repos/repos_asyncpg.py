from contextlib import contextmanager
from typing import Any, AsyncGenerator

import asyncpg
import orjson

from apps.core.models import TestData
from apps.core.repos.repo_base import RepoBase
from project.configs import db_settings


class AsyncPGContextManager:
    def __init__(self):
        self.conn = None

    async def __aenter__(self):
        self.conn = await asyncpg.connect(**db_settings.get_bouncer_params())
        return self.conn

    async def __aexit__(self, exc_type, exc, tb):
        await self.conn.close()


class RepoAsyncPG(RepoBase):
    _asyncpg_poll: asyncpg.pool.Pool | None = None

    def __init__(self):
        super().__init__()
        self._asyncpg_poll = None

    async def _add_data_to_db(self, data: list[dict[str, str]]) -> None:
        # операции не требующие высокой производительности оставим на orm
        await TestData.objects.abulk_create((TestData(**item) for item in data))

    async def get_batch_data(
        self, from_index: int, batch_size: int = 10_000
    ) -> list[dict[str, Any]]:
        try:
            list_result = []
            async with AsyncPGContextManager() as conn:
                rows = await conn.fetch(
                    'SELECT id, payload::text as payload_str FROM core_testdata WHERE id > $2 ORDER BY id LIMIT $1 ',
                    batch_size,
                    from_index,
                )
                list_result.extend(
                    {'index': row['id'], 'data': row['payload_str']} for row in rows
                )
            return list_result
        except Exception as ex:
            self._logger.exception(ex)
            raise

    async def add_data(self, data: list[dict[str, str]]) -> bool:
        try:
            async with AsyncPGContextManager() as conn:
                await conn.executemany(
                    '''
                    INSERT INTO core_testdata(payload) VALUES ($1)
                    ''',
                    ((orjson.dumps(item).decode('utf-8'),) for item in data),
                )

            return True
        except Exception as ex:
            self._logger.exception(ex)
            return False

    async def clear_table(self) -> None:
        await TestData.objects.all().adelete()
