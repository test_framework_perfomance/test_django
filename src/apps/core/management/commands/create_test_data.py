import asyncio

from django.core.management.base import BaseCommand

from apps.core.repos.repo_base import get_repo_json


class Command(BaseCommand):
    help = 'Создать тестовые данные'

    def add_arguments(self, parser):
        parser.add_argument(
            'count_items', type=int, help='Количество строк тестовых данных'
        )
        parser.add_argument('count_columns', type=int, help='Количество колонок в json')

    async def create_test_data(self, count_items: int, count_columns: int) -> None:
        repo = get_repo_json()
        await repo.generate_data(count_lines=count_items, count_columns=count_columns)

    def handle(self, *args, **options):
        asyncio.run(
            self.create_test_data(
                count_items=options['count_items'],
                count_columns=options['count_columns'],
            )
        )
        self.stdout.write(
            self.style.SUCCESS('Создание тестовых данных завершено успешно.')
        )
