import asyncio

from django.core.management.base import BaseCommand

from apps.core.repos.repo_base import get_repo_json


class Command(BaseCommand):
    help = 'Создать тестовые данные'

    async def clear_test_data(self) -> None:
        repo = get_repo_json()
        await repo.clear_table()

    def handle(self, *args, **options):
        asyncio.run(self.clear_test_data())
        self.stdout.write(self.style.SUCCESS('Очистка данных завершена успешно.'))
