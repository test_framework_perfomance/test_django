import json
from http import HTTPStatus
from typing import Any

import orjson
from django.http import JsonResponse
from rest_framework.response import Response


def get_response(
    meta: dict[str, Any] | None = None,
    data: Any = None,
    status: HTTPStatus = HTTPStatus.OK,
) -> Response:
    meta = meta or {'msg': 'ok'}
    data = data or {}

    return JsonResponse(
        data={'meta': meta, 'data': data}, status=status, encoder=OrjsonEncoder
    )


class OrjsonEncoder(json.JSONEncoder):
    def default(self, obj) -> str:
        return orjson.dumps(obj).decode('utf-8')
