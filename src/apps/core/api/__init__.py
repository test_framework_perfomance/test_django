from django.urls import include, path

from apps.core.api.v1 import v1_urls

api_urls = [path('v1/', include(v1_urls))]
