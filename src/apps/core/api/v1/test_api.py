from http import HTTPStatus

from adrf.views import APIView
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response

from apps.core.api.utils_response import get_response
from apps.core.repos.repo_base import get_repo_json


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class AsyncViewTest(APIView):
    authentication_classes = [CsrfExemptSessionAuthentication]
    permission_classes = []
    throttle_classes = []

    async def get(self, request) -> Response:
        repo_pg = get_repo_json()
        from_index = int(request.query_params.get('from_index', 0))
        batch_size = int(request.query_params.get('batch_size', 100))
        data = await repo_pg.get_batch_data(
            from_index=from_index, batch_size=batch_size
        )

        return get_response(data=data)

    async def post(self, request) -> Response:
        input_data = request.data.get('data_input', [])
        success = True
        if input_data:
            repo_pg = get_repo_json()
            success = await repo_pg.add_data(input_data)
        if success:
            return get_response(meta={'msg': 'данные успешно вставлены.'})
        return get_response(
            meta={'msg': 'Во время вставки данных произошла ошибка'},
            status=HTTPStatus.BAD_REQUEST,
        )
