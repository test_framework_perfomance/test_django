from django.urls import path

from apps.core.api.v1.test_api import AsyncViewTest

v1_urls = [
    path('test_data', AsyncViewTest.as_view(), name='test_data'),
]
