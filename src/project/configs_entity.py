from pydantic_settings import BaseSettings, SettingsConfigDict


class MyBaseSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_file='.env', env_file_encoding='utf-8', extra="ignore"
    )


class DBSettings(MyBaseSettings):
    model_config = SettingsConfigDict(env_prefix="BACKEND_DB_")

    HOST: str
    BOUNCER_HOST: str
    PORT: int
    BOUNCER_PORT: int
    USER: str
    PASSWORD: str
    NAME: str

    def get_dsn(self) -> str:
        return f"postgresql://{self.USER}:{self.PASSWORD}@{self.HOST}:{self.PORT}/{self.NAME}"

    def get_bouncer_params(self) -> dict[str, str | int]:
        return {
            "user": self.USER,
            "password": self.PASSWORD,
            "host": self.BOUNCER_HOST,
            "port": self.BOUNCER_PORT,
            "database": self.NAME,
        }
