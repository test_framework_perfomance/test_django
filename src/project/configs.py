import os
import pathlib

from project.configs_entity import DBSettings

_base_to_env = pathlib.Path(__file__).parents[2] / 'dc_django_test' / '.env'
if _base_to_env.exists():
    from dotenv import load_dotenv

    load_dotenv(_base_to_env, override=True)


db_settings = DBSettings()

IS_DEBUG = os.environ.get('DEBUG', 'True').upper() in ('TRUE', '1')
SECRET_KEY = os.environ.get('SECRET_KEY')
