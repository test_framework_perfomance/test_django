#!/bin/bash
filename="/app/volumes/data_is_created.txt"

if [ ! -f "$filename" ]; then
    python manage.py migrate
    echo "Data is not created. Creating..."
    python manage.py create_test_data $TEST_COUNT_LINES $TEST_COUNT_COLUMN_IN_JOSN
    touch "$filename"
fi

gunicorn -k uvicorn.workers.UvicornWorker project.asgi:application --preload --max-requests 1200 --max-requests-jitter 300 --bind 0.0.0.0:8000 --workers 4
